//
//  Petition.swift
//  Project7
//
//  Created by JONATHAN HOLLAND on 7/12/19.
//  Copyright © 2019 JONATHAN HOLLAND. All rights reserved.
//

import Foundation

struct Petition: Codable {
    var title: String
    var body: String
    var signatureCount: Int
}
