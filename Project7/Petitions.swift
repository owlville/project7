//
//  Petitions.swift
//  Project7
//
//  Created by JONATHAN HOLLAND on 7/12/19.
//  Copyright © 2019 JONATHAN HOLLAND. All rights reserved.
//

import Foundation

struct Petitions: Codable {
    var results: [Petition]
}
